# usage

```
# pip3 install --user pipenv
pipenv --python 3.8.3
pipenv install
pipenv run gunicorn -b0.0.0.0:8000 mazes:app
```

```
python3 -m venv venv
. venv/bin/activate
pip install -r requirements/requirements.txt
```

```
oc new-app https://gitlab.com/modle13/maze-generator.git
oc expose svc/maze-generator
```
