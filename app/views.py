from flask import render_template, request, Response
import json
from random import randrange, shuffle

from app import app

some_data = {}

WIDTH = 20
HEIGHT = 20


@app.route('/', methods=['GET', 'POST'])
def main():
    if request.method == 'POST':
        if request.form.get('download') == 'DOWNLOAD':
            print('that would be a download')
        elif request.form.get('generate') == 'GENERATE':
            print('that would be a generate')
        else:
            print('what')

    some_data['maze'] = generate_maze()
    return render_template('index.html', data=some_data)


@app.route('/download', methods=['GET'])
def download():
    return Response(
        some_data.get('maze'),
        mimetype="text/plain",
        headers={"Content-disposition":
                 "attachment; filename=themaze.txt"})


def generate_maze(w=WIDTH, h=HEIGHT):
    vis = [[0] * w + [1] for _ in range(h)] + [[1] * (w + 1)]
    ver = [["|  "] * w + ['|'] for _ in range(h)] + [[]]
    hor = [["+--"] * w + ['+'] for _ in range(h + 1)]

    def walk(x, y):
        vis[y][x] = 1

        d = [(x - 1, y), (x, y + 1), (x + 1, y), (x, y - 1)]
        shuffle(d)
        for (xx, yy) in d:
            if vis[yy][xx]: continue
            if xx == x: hor[max(y, yy)][x] = "+  "
            if yy == y: ver[y][max(x, xx)] = "   "
            walk(xx, yy)

    walk(randrange(w), randrange(h))

    s = ""
    for (a, b) in zip(hor, ver):
        s += ''.join(a + ['\n'] + b + ['\n'])
    return s
