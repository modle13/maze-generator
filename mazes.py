from app import app

# needed for openshift demo since Dockerfile runs this file directly
# would normally use gunicorn run command or similar web server
if __name__ == '__main__':  # Script executed directly?
    print("Hello World! Built with a Docker file.")
    app.run(host="0.0.0.0", port=5000, debug=True,use_reloader=True)  # Launch built-in web server and run this Flask webapp
